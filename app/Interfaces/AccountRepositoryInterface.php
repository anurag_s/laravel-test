<?php

namespace App\Interfaces;

interface AccountRepositoryInterface
{
    public function all();
    public function create($data);
    public function find($id);
    public function update($id, $data);
    public function delete($id);
    public function getAccounts($request);
    public function getRelatedContacts($id);
}