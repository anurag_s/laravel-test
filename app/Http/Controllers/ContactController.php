<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Interfaces\ContactRepositoryInterface;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ContactController extends Controller
{
    public function __construct(ContactRepositoryInterface $contact)
    {
        $this->contactRepo = $contact;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$contact = Contact::latest()->get();
        $this->authorize('view', Contact::class);
        if(request()->ajax())
        {
            //$contacts = Contact::select(['first_name', 'last_name', 'email', 'phone', 'designation', 'department', 'hierarchy', 'customer_type', 'assigned_to', 'created_at', 'updated_at']);
            $contacts = Contact::all();
            return DataTables::of($contacts)
                ->addColumn('name', function($contact) {
                    return '<a href="#">'. $contact->first_name . " " . $contact->last_name. '</a>';
                })
                ->removeColumn('first_name')
                ->removeColumn('last_name')
                ->addColumn('account_name', function($contact){
                    // foreach($contacts as $contact) 
                    // {
                        foreach($contact->accounts as $account)
                        {
                            return '<a href="'.route('accounts.show', $account->id).'">'.$account->account_name.'</a>';
                        }
                    // }  
                    //return $contact->accounts;
                })->rawColumns(['name', 'account_name'])   
                ->make();
                //dd($contact);
        }
        //$contacts = Contact::all();
        return view('contacts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Contact::class);
        return view('contacts.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        //dd(dump($request->all('followup_date')));
        $this->contactRepo->create($request);
        return redirect()->route('contacts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
