<?php

namespace App\Http\Controllers;

use App\Account;
use App\Contact;
use App\Interfaces\AccountRepositoryInterface;
use App\Jobs\MassDeleteAccounts;
use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Yajra\DataTables\DataTables;

class AccountController extends Controller
{
    public function __construct(AccountRepositoryInterface $account)
    {
        $this->accountRepo = $account;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view', Account::class);
        if(request()->ajax())
        {
            $account = Account::all();
            return DataTables::of($account)
                ->addColumn('mass_actions', function($account){
                    return '<input type="checkbox" class="mass-update-check" value="'.$account->id.'">';
                })
                ->addColumn('column_selection', function($account){
                    return '<div class="edit-and-delete"><a href="'.route('accounts.edit', $account->id).'" class="edit-pencil">'.'<i class="fa fa-pencil edit-pencil" style="font-size:20px"></i>'.'</a>'.
                    '<form action="'.route('accounts.destroy', $account->id).'" method="POST">
                    '.csrf_field().'
                    '.method_field("DELETE").'
                    <button type="submit" class = "delete-btn"
                        onclick="return confirm(\'Are You Sure Want to Delete?\')"
                        ><i class="fa fa-trash delete-trash" style="font-size:20px"></i></button>
                    </form></div>';
                })
                ->editColumn('account_name', function($account) {
                    return '<a href="'.route('accounts.show', $account->id).'">'.$account->account_name.'</a>';
                })->rawColumns(['account_name', 'mass_actions', 'column_selection'])
                ->make();
        }
        return view('accounts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Account::class);
        return view('accounts.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->accountRepo->create($request);
        return redirect()->route('accounts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('viewAny', Account::class);
        return view('accounts.show', ['account' => $this->accountRepo->find($id)]);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('accounts.edit', ['account' => $this->accountRepo->find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->accountRepo->update($id, $request);
        return redirect()->route('accounts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->accountRepo->delete($id);
        return redirect()->route('accounts.index');
    }

    public function getAccounts(Request $request)
    {
        return $this->accountRepo->getAccounts($request);
        //return response()->json($response);
    }

    public function getRelatedContacts($accountId)
    {
        return $this->accountRepo->getRelatedContacts($accountId);
    }

    public function massUpdateAccounts(Request $request)
    {
        $ids = $request->input('ids');
    }

    public function massDeleteAccounts(Request $request)
    {
        $ids = $request->input('ids');
        MassDeleteAccounts::dispatch($ids);
        //dispatch($ids);
        //return redirect()->route('accounts.index');
    }
}
