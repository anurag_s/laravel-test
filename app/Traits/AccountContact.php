<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

trait AccountContact 
{
    protected static function bootUuids()
    {
        static::saved(function ($model) {
            $accountId = request()->all('account_id')['account_id'];
            $contactId = $model->getKey();
            if(DB::table('account_contact')
                ->where('account_id', request()->all('account_id'))
                ->where('contact_id', $model->getKey())
                ->count() < 1)
            {
                $contact = $model::find($contactId);
                $contact->accounts()->attach($accountId);
            }
        });
    }
}