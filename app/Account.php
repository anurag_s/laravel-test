<?php

namespace App;

use Illuminate\Database\Schema\Builder;

//Builder
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use Uuids, SoftDeletes;

    protected $fillable = [
        'account_name',
        'account_relationship',
        'phone',
        'email',
        'industry',
        'website',
        'linkedln_url',
        'gstin',
        'pan',
        'account_status',
        'description',
        'comments',
        'cosummary',
        'assigned_to'
    ];

    public function contacts() 
    {
        return $this->belongsToMany(Contact::class);
    }
}
