<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'user_role', 'allowed_modules', 'allowed_operations', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //used without package for roles
    // public function isAdmin()
    // {
    //     return $this->user_role === 'admin';
    // }

    // public function isUser()
    // {
    //     return $this->user_role === 'user';
    // }

    public function isModuleAllowed($moduleName)
    {
        $modulesAllowed = explode(',', $this->allowed_modules);
        return in_array($moduleName, $modulesAllowed);
    }
}
