<?php

namespace App;

use App\Traits\AccountContact;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use Uuids, SoftDeletes, AccountContact;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'wa_verification',
        'customer_type',
        'lead_source',
        'designation',
        'department',
        'hierarchy',
        'legend_code',
        'address_verified',
        'followup_date',
        'description',
        'comment',
        'assigned_to',
    ];

    public function accounts()
    {
        return $this->belongsToMany(Account::class);
    }
}
