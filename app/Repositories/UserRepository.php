<?php

namespace App\Repositories;

use App\User;
use App\Interfaces\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;

class UserRepository implements UserRepositoryInterface
{
    public function all()
    {   
        return User::all();
    }

    public function create($data)
    {
        //get the request and assign appropriate values before mass assign
        //dd($data->all());
        $data['allowed_modules'] = implode(',' , $data['allowed_modules']);
        $data['password'] = Hash::make($data['password']);
        return User::create($data->all());
    }

    public function find($id)
    {
        
    }

    public function update($id, $data)
    {
        
    }

    public function delete($id)
    {
        
    }
}