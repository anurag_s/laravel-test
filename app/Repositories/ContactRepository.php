<?php

namespace App\Repositories;

use App\Contact;
use App\Interfaces\ContactRepositoryInterface;

class ContactRepository implements ContactRepositoryInterface
{
    public function all()
    {   
        return Contact::all();
    }

    public function create($data)
    {
        return Contact::create($data->all());
    }

    public function find($id)
    {
        
    }

    public function update($id, $data)
    {
        
    }

    public function delete($id)
    {
        
    }
}