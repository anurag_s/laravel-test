<?php

namespace App\Repositories;

use App\Account;
use App\Interfaces\AccountRepositoryInterface;
use Yajra\DataTables\DataTables;

class AccountRepository implements AccountRepositoryInterface
{
    public function all()
    {   
        return Account::all();
    }

    public function create($data)
    {
        //dd($data->all());
        return Account::create($data->all());
    }

    public function find($id)
    {
        $account = Account::find($id);
        return $account;
    }

    public function update($id, $data)
    {
        $account = Account::find($id);
        $account->fill($data->all())->save();
        return $account;
    }

    public function delete($id)
    {
        return Account::find($id)->delete();
    }

    public function getAccounts($request)
    {
        $search = $request->search;

        if($search == ''){
           $accounts = Account::orderby('account_name','asc')->select('id','account_name')->get();
        }else{
           $accounts = Account::orderby('account_name','asc')->select('id','account_name')->where('account_name', 'like', '%' .$search . '%')->limit(5)->get();
        }
  
        $response = array();
        foreach($accounts as $account){
           $response[] = array("value"=>$account->id,"label"=>$account->account_name);
        }
        
        return response()->json($response);
    }

    public function getRelatedContacts($id)
    {
        $contacts = Account::find($id)->contacts()->orderBy('first_name')->get();
        return DataTables::of($contacts)
            ->addColumn('name', function($contact) {
                return '<a href="#">'. $contact->first_name . " " . $contact->last_name. '</a>';
            })
            ->removeColumn('first_name')
            ->removeColumn('last_name')
            ->addColumn('account_name', function($contact){
                    foreach($contact->accounts as $account)
                    {
                        return '<a href="'.route('accounts.show', $account->id).'">'.$account->account_name.'</a>';
                    }
            })
            ->rawColumns(['name', 'account_name'])   
            ->make();
    }
}