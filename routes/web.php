<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Hello\HelloFacade as Hello;
use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/home', function () {
    return view('test');
})->middleware('auth'); //need to login to access this route


// //Route::redirect('foo', 'baz'); //used to redirect to a specific route

// Route::get('foo', function () {
//     return Auth::user();
// })->name('anurag');


Route::view('/testview', 'testview');   //just returns a view

// Route::get('test/{data}', 'TestController@testPrint'); //calls a method in controller along with parameter

// Route::get('user/{id}', function($id){ //  route parameters used to capture segements form the uri  
//     return 'user'. $id;
// });

// Route::get('user/{id}/content/{name}', function($id, $name){ //can capture more than one parameter
//     return "id: " . $id . " name: " . $name;
//     //return redirect()->route('anurag');//this will redirect to route named 'anurag'

// });

// Route::get('movie/{id}/actor/{name?}', function($id, $name = null){ // parameters can also be defualt and optional
//     return "id: " . $id . " name: " . $name;
// })->where('id', '[0-9]+'); //constrain the format of your route parameters using regular expressions

// Route::get('');
Auth::routes(['register' => true]); //disables register new users

Route::post('accounts/getAccounts/','AccountController@getAccounts')->name('accounts.getAccounts');
Route::get('accounts/getRelatedContacts/{accountId}','AccountController@getRelatedContacts')->name('accounts.getRelatedContacts');
Route::post('accounts/massUpdateAccounts/','AccountController@massUpdateAccounts')->name('accounts.massUpdateAccounts');
Route::post('accounts/massDeleteAccounts/','AccountController@massDeleteAccounts')->name('accounts.massDeleteAccounts');

//written above the resource routes


Route::resource('accounts', 'AccountController')->middleware('auth');  
Route::resource('contacts', 'ContactController')->middleware('auth');
//role using spatie   
// Route::resource('users', 'UserController')->middleware(['auth', 'can:create-users', 'can:delete-users', 'can:edit-users']);  

Route::resource('users', 'UserController')->middleware('auth');  

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/test', function() {
    //Hello::helloInHindi(); using facade
    //App::make('hello')->helloInHindi(); //without using facade
    app('hello')->helloInHindi(); // without facade and using app() helper
});