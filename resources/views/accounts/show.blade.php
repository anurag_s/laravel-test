@extends('layouts.app')

@section('links')
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css"> 
@endsection
@section('content')

    {{-- <h1>{{ $account->account_name }}</h1> --}}

    <div class="panel">
        <button class="collapsible panel-head">Contacts</button>
        <div class="content panel-body">
            <div class="table_account_contact">
                <table class="table table-bordered account_contact_datatable" width="100%">
                    <thead>
                        <tr>
                            <th>Contact Name</th>
                            <th>Account Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Designation</th>
                            <th>Department</th>
                            <th>Hierarchy</th>
                            <th>Customer Type</th>
                            <th>Assigned To</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        
    </div>
    
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js" ></script>
<script>
    $( ".panel-head" ).click(function() {
      //alert('s');
      //alert('{{$account->id}}');

      $.noConflict();
      var table = $('.account_contact_datatable').DataTable({
            processing: true,
            serverSide: true,
            // ajax: {
            //     url: "{{ route('accounts.index') }}",
            //     //dataSrc: 'data'
            // },
            ajax:  {
                url: "{{url('accounts/getRelatedContacts')}}"+'/'+'{{$account->id}}',
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'account_name', name: 'account_name'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'designation', name: 'designation'},
                {data: 'department', name: 'department'},
                {data: 'hierarchy', name: 'hierarchy'},
                {data: 'customer_type', name: 'customer_type'},
                {data: 'assigned_to', name: 'assigned_to'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'},
            ],
        scrollX: true,
        scrollY: '170px',
        }); 
    });
    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var content = this.nextElementSibling;
            if (content.style.maxHeight){
            content.style.maxHeight = null;
            } else {
            content.style.maxHeight = content.scrollHeight + "5px";
            } 
        });
    }
</script>
@endsection