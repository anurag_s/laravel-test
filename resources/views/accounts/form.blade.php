{!! Form::submit('Save', ['class' => 'btn btn-save']) !!}

<a href="{{ route('accounts.index') }}" class="btn btn-warning btn-cancel">Cancel</a>
<div class="form">

    <div class="form-group{{ $errors->has('account_name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'Account Name') !!}
        {!! Form::text('account_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('account_name') }}</small>
    </div>

   <div class="form-group{{ $errors->has('account_relationship') ? ' has-error' : '' }}">
   {!! Form::label('account_relationship', 'Account Relation') !!} <br>
   {!! Form::select('account_relationship', ['' => '', 'Customer' => 'Customer', 'Aggregate Partner' => 'Aggregate Partner', 'Competitor' => 'Competitor', 'Courier Company' => 'Courier Company', 'End User' => 'End User', 'Evolve Partner' => 'Evolve Partner', 'Partner' => 'Partner', 'Supplier' => 'Supplier'], null, ['id' => 'account_name', 'required' => 'required', 'class' => 'form-control select']) !!}
   <small class="text-danger">{{ $errors->first('account_relationship') }}</small>
   </div>

    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
        {!! Form::label('phone', 'Phone') !!}
        {!! Form::text('phone', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('phone') }}</small>
    </div>

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        {!! Form::label('email', 'Email') !!}
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('email') }}</small>
    </div>

    <div class="form-group{{ $errors->has('industry') ? ' has-error' : '' }}">
        {!! Form::label('industry', 'Industry') !!} <br>
        {!! Form::select('industry', ['' => '', 'Advertising n Media' => 'Advertising & Media', 'Associations' => 'Associations', 'Automobile' => 'Automobile', 'Automobile_Retail' => 'Automobile-Retail', 'Banking_NBFC' => 'Banking /NBFC', 'Business Coach Consultant' => 'Business Coach Consultant', 'CallCenterBPO' => 'Call Center BPO', 'Consulting_CA_Tax' => 'Consulting, CA, Tax', 'E-Commerce' => 'E-Commerce'], null, ['class' => 'form-control select']  ) !!}
        <small class="text-danger">{{ $errors->first('industry') }}</small>
    </div>

    <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
        {!! Form::label('website', 'Website') !!}
        {!! Form::url('website', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('website') }}</small>
    </div>

    <div class="form-group{{ $errors->has('linkedln_url') ? ' has-error' : '' }}">
        {!! Form::label('linkedln_url', 'Linkedin Url') !!}
        {!! Form::url('linkedln_url', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('linkedln_url') }}</small>
    </div>

    <div class="form-group{{ $errors->has('gstin') ? ' has-error' : '' }}">
        {!! Form::label('gstin', 'GSTIN') !!}
        {!! Form::text('gstin', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('gstin') }}</small>
    </div>

    <div class="form-group{{ $errors->has('pan') ? ' has-error' : '' }}">
        {!! Form::label('pan', 'PAN') !!}
        {!! Form::text('pan', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('pan') }}</small>
    </div>

    <div class="form-group{{ $errors->has('account_status') ? ' has-error' : '' }}">
        {!! Form::label('account_status', 'Account Status') !!} <br>
        {!! Form::select('account_status', ['' => '', 'Active' => 'Active', 'Inactive' => 'Inactive'], null, ['required' => 'required', 'class' => 'form-control select']) !!}
        <small class="text-danger">{{ $errors->first('account_status') }}</small>
    </div>

    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        {!! Form::label('description', 'Description / Summary') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control description', 'rows' => '3']) !!}
        <small class="text-danger">{{ $errors->first('description') }}</small>
    </div>


    <div class="form-group{{ $errors->has('comments') ? ' has-error' : '' }}">
        {!! Form::label('comments', 'Comments / History') !!}
        {!! Form::textarea('comments', null, ['class' => 'form-control comments', 'rows' => '3']) !!}
        <small class="text-danger">{{ $errors->first('comments') }}</small>
    </div>


    <div class="form-group{{ $errors->has('cosummary') ? ' has-error' : '' }}">
        {!! Form::label('cosummary', 'Cosummary') !!}
        {!! Form::textarea('cosummary', null, ['class' => 'form-control cosummary', 'rows' => '3']) !!}
        <small class="text-danger">{{ $errors->first('cosummary') }}</small>
    </div>

    <div class="form-group{{ $errors->has('assigned_to') ? ' has-error' : '' }}">
    {!! Form::label('assigned_to', 'Assigned To') !!}
    {!! Form::text('assigned_to', null, ['class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('assigned_to') }}</small>
    </div>
    
</div>
