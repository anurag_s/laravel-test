@extends('layouts.app')
@section('content')
{!! Form::open(['route' => 'accounts.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
@csrf
@include('accounts.form')
{!! Form::close() !!}
@endsection