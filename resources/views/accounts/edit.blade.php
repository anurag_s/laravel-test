@extends('layouts.app')
@section('content')
    {{ Form::model($account, ['route' => ['accounts.update', $account->id ]]) }}
    @csrf
    @method('PATCH')
    @include('accounts.form')
    {{ Form::close() }}
@endsection