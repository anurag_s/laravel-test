@extends('layouts.app')

@section('links')
 <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css"> 
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection
@section('content')
<div class="container">
        <div class="mass-options">
            <button class="mass-update-btn" title="Mass Update">
                <i class="fa fa-pencil edit-pencil mass-update-pencil" style="font-size:20px"></i>
            </button>
            <button class="mass-delete-btn" title="Mass Delete">
                <i class="fa fa-trash delete-trash mass-delete-trash" style="font-size:20px"></i>
            </button>
        </div>
    <table class="table table-bordered data-table" width="100%">
        <thead>
            <tr>
                <th>
                    <input type="checkbox">
                    <button type="button" class="btn dropdown-toggle dropdown-toggle-split center-flex mass-action-drop">
                    </button>
                </th>
                <th></th>
                <th>Account Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Account Relationship</th>
                <th>Industry</th>
                <th>Website</th>
                <th>GSTIN</th>
                <th>Assigned To</th>
                <th>LinkedIn Profile</th>
                <th>Account Status</th>
                <th>Created At</th>
                <th>Updated At</th>
            </tr>
        </thead>
        {{-- <thead class="columnSearch">
            <tr>
                <th class="searchable">Name</th>
                <th class="searchable">Phone</th>
                <th class="searchable">Email</th>
                <th class="searchable">Relationship</th>
                <th class="searchable">Relationship</th>
                <th class="searchable">Relationship</th>
                <th class="searchable">Industry</th>
                <th class="searchable">Website</th>
                <th class="searchable">GSTIN</th>
                <th class="searchable">Assigned To</th>
                <th class="searchable">LinkedIn</th>
                <th class="searchable">Account Status</th>
                <th class="searchable">Created At</th>
                <th class="searchable">Created At</th>
                <th class="searchable">Updated At</th>
            </tr>
        </thead> --}}
        <tbody>
            
        </tbody>
    </table>
</div>
@endsection
@section('scripts')
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js" ></script>
<script> 
    //  $('.data-table .columnSearch th.searchable').each(function () {
    //     var title = $(this).text();
    //     $(this).html('<input type="text" placeholder="Search ' + title + '" />');
    // });
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
     var table = $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            // ajax: {
            //     url: "{{ route('accounts.index') }}",
            //     //dataSrc: 'data'
            // },
            ajax:  "{{ route('accounts.index') }}",
            columns: [
                {data: 'mass_actions', name: 'mass_actions',  orderable: false, searchable: false},
                {data: 'column_selection', name: 'column_selection',  orderable: false, searchable: false},
                {data: 'account_name', name: 'account_name'},
                {data: 'phone', name: 'phone'},
                {data: 'email', name: 'email'},
                {data: 'account_relationship', name: 'account_relationship'},
                {data: 'industry', name: 'industry'},
                {data: 'website', name: 'website'},
                {data: 'gstin', name: 'gstin'},
                {data: 'assigned_to', name: 'assigned_to'},
                {data: 'linkedln_url', name: 'linkedln_url'},
                {data: 'account_status', name: 'account_status'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'},
            ],
            scrollX: true,
            "targets": 'no-sort',
            "bSort": true,
            "order": []
        });

        // $( ".edit-pencil" ).hover(function() {
        //     $( "<i>" ).fadeOut( 100 );
        //     $( this ).fadeIn( 500 );
        // });

        $('.mass-options').hide();
        //$('.mass-update-delete').hide();

        $(document).on('change' , '.mass-update-check', function(){
            $('.mass-options')[$('input[type=checkbox]:checked').length >= 1 ? 'slideDown' : 'slideUp']('fast');
        });

        $('.mass-update-btn').on('click', function() {

            //console.log(massIDs);
             var massIDs = $('input:checkbox:checked').map(function(){
                return $(this).val();
            }).toArray();
            $.ajax({
            url:"{{route('accounts.massUpdateAccounts')}}",
            type: 'post',
            dataType: "json",
            data: {
               _token: CSRF_TOKEN,
               ids: massIDs
            },
            success: function(response) {
                table.draw();
            }
          });
        });

        $('.mass-delete-btn').on('click', function() {
            var confirmed = confirm('Are You Sure Want to Delete?');
            var massIDs = $('input:checkbox:checked').map(function(){
                return $(this).val();
            }).toArray();
            //console.log(massIDs);
            if(confirmed) {
                $.ajax({
                    url:"{{route('accounts.massDeleteAccounts')}}",
                    type: 'post',
                    dataType: "json",
                    data: {
                    _token: CSRF_TOKEN,
                    ids: massIDs
                    },
                    success: function(response) {
                    table.draw();
                    $('.mass-options').hide();
                    }
                });
            }
            else
            {
                $('.mass-options').hide();
                $('input:checkbox').prop('checked',false);
            }
        });

</script>

@endsection