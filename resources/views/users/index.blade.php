@extends('layouts.app')

@section('links')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
@endsection
@section('content')
    <div class="container">
        <table class="table table-bordered data-table" width="100%">
            <thead>
                <tr>
                    <th>id</th>
                    <th>User Name</th>
                    <th>Email</th>
                 </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td> {{ $user->id }} </td>
                        <td> {{ $user->name }} </td>
                        <td> {{ $user->email }} </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script>
        //  var table = $('.data-table').DataTable({
        //         processing: true,
        //         serverSide: false,
        //         // ajax: {
        //         //     url: "{{ route('accounts.index') }}",
        //         //     //dataSrc: 'data'
        //         // },
        //         ajax:  "{{ route('contacts.index') }}",
        //         columns: [
        //             {data: 'name', name: 'name'},
        //             {data: 'account_name', name: 'account_name'},
        //             {data: 'email', name: 'email'},
        //             {data: 'phone', name: 'phone'},
        //             {data: 'designation', name: 'designation'},
        //             {data: 'department', name: 'department'},
        //             {data: 'hierarchy', name: 'hierarchy'},
        //             {data: 'customer_type', name: 'customer_type'},
        //             {data: 'assigned_to', name: 'assigned_to'},
        //             {data: 'created_at', name: 'created_at'},
        //             {data: 'updated_at', name: 'updated_at'},
        //         ],
        //     scrollX: true,
        //     });
    </script>
@endsection
