@extends('layouts.app')

@section('links')
@endsection

@section('content')
{!! Form::open(['route' => 'users.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
@csrf
@include('users.form')
{!! Form::close() !!}
@endsection

@section('scripts')
<script>
    $('.user_role_select').on('change', function() {
        if(this.value == 'admin')
        {
            $('input[value="account"]').prop('checked', true);
            $('input[value="contact"]').prop('checked', true);
            $('input[value="user"]').prop('checked', true);
        }

        if(this.value == 'user')
        {
            $('input[value="account"]').prop('checked', true);
            $('input[value="contact"]').prop('checked', true);
            $('input[value="user"]').prop('checked', false);
        }
    });
</script>
@endsection
