{!! Form::submit('Save', ['class' => 'btn btn-save']) !!}
<a href="{{ route('users.index') }}" class="btn btn-warning btn-cancel">Cancel</a>
<div class="form">

    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'User Name') !!}
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('name') }}</small>
    </div>

   <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
   {!! Form::label('email', 'Email') !!} <br>
   {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
   <small class="text-danger">{{ $errors->first('email') }}</small>
   </div>

   <div class="form-group{{ $errors->has('user_role') ? ' has-error' : '' }}">
    {!! Form::label('user_role', 'Role') !!} <br>
    {!! Form::select('user_role', ['' => '', 'admin' => 'Admin', 'user' => 'User'], ['id' => 'account_name', 'required' => 'required'], ['class' => 'form-control select user_role_select']) !!}
    <small class="text-danger">{{ $errors->first('user_role') }}</small>
    </div>

    <div class="form-group">
    <div class="checkbox{{ $errors->has('allowed_modules') ? ' has-error' : '' }}">
    <label for="allowed_modules">Allowed Modules:</label><br>
    <label for="account">
    {!! Form::checkbox('allowed_modules[]', 'account', null, ['id' => 'allowed_modules']) !!} Account
    </label><br>

    <label for="contact">
        {!! Form::checkbox('allowed_modules[]', 'contact', null, ['id' => 'allowed_modules']) !!} Contact
    </label><br>

    <label for="user">
        {!! Form::checkbox('allowed_modules[]', 'user', null, ['id' => 'allowed_modules']) !!} User
    </label><br>
        
    </div>
    <small class="text-danger">{{ $errors->first('checkbox_id') }}</small>
    </div>

   <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        {!! Form::label('password', 'Password') !!} <br>
        {!! Form::password('password', ['class' => 'form-control', 'id' => 'password']) !!}
        <small class="text-danger">{{ $errors->first('password') }}</small>
    </div>

    <div class="form-group{{ $errors->has('confirm_password') ? ' has-error' : '' }}">
        {!! Form::label('confirm_password', 'Confirm Password') !!}
        {!! Form::password('confirm_password', ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('confirm_password') }}</small>
    </div>

    