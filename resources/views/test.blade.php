{{-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    @auth // 
        {{  "the user is authenticated" }}
    @endauth

    @guest
        {{ "the user is not authenticated" }}
    @endguest

    @unless (Auth::check())
    {{ "the user is not authenticated" }}
    @endunless


</body>
</html> --}}

@extends('master.app')

@section('title', 'Test page title')

@section('content')
    <a href="" class="btn btn-info pull-right">Test</a>
@endsection