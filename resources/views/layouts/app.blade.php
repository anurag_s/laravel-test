<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    @yield('links')

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm navbar-above">
            <div class="container">
                {{-- <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a> --}}
                @auth
                {{-- <input class="form-control" placeholder="Search.." id="global-search-input" name="query" type="text" value="" autocomplete="off"> --}}
                @endauth
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        @guest
                        <!-- Authentication Links -->
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
       @auth
       <div id="sidebar-close">
        <button class="openbtn" onclick="openNav()">☰</button>  
      </div>
       <div id="mySidebar" class="sidebar">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        {{-- <div class="account-dropdown">
            <a href="{{ url('accounts') }}" class="account-link">Accounts</a>
            <div class="account-dropdown-content">
                <a href="{{ url('accounts/create') }}">Create</a>
            </div>
        </div> --}}
        <ul class="sidebar-modulelist">
            <div class="sidebar-list">
              @can('view', App\Account::class)
              <li>
                <div class="btn-group sidebar-module">
                    <a class="btn btn-danger" href="{{ url('accounts') }}">Account</a>
                    <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="{{ url('accounts/create') }}">Create</a>
                    </div>
                  </div>            
              </li>
              @endcan
              @can('view', App\Contact::class)
                <li>
                    <div class="btn-group sidebar-module">
                        <a class="btn btn-danger" href="{{ url('contacts') }}">Contact</a>
                        <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="{{ url('contacts/create') }}">Create</a>
                        </div>
                      </div>            
                </li>
              @endcan
                {{-- roles using spatie --}}
                {{-- @can(['create-users', 'edit-users', 'delete-users'])
                <li>
                    <div class="btn-group sidebar-module">
                        <a class="btn btn-danger" href="{{ url('users') }}">&nbsp;User&nbsp;&nbsp;&nbsp;&nbsp;</a>
                        <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="{{ url('users/create') }}">Create</a>
                        </div>
                      </div>            
                </li>
                @endcan --}}
                {{-- @admin
                <li>
                    <div class="btn-group sidebar-module">
                        <a class="btn btn-danger" href="{{ url('users') }}">&nbsp;User&nbsp;&nbsp;&nbsp;&nbsp;</a>
                        <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="{{ url('users/create') }}">Create</a>
                        </div>
                      </div>            
                </li>
                @endadmin --}}
                @can('view', App\User::class)
                <li>
                  <div class="btn-group sidebar-module">
                      <a class="btn btn-danger" href="{{ url('users') }}">&nbsp;User&nbsp;&nbsp;&nbsp;&nbsp;</a>
                      <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ url('users/create') }}">Create</a>
                      </div>
                    </div>            
                </li>
                @endcan
            </div>
        </ul>
         
      </div>
      
       @endauth
        <main class="py-4">
            @yield('content')
        </main>
        <script>
          function openNav() {
            document.getElementById("mySidebar").style.width = "250px";
            document.getElementById("main").style.marginLeft = "250px";
          }

          function closeNav() {
            document.getElementById("mySidebar").style.width = "0";
            document.getElementById("main").style.marginLeft= "0";
          }
        </script>
    </div>
    <script src="{{ asset('js/min.js') }}"></script>
    @yield('scripts')
    <script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>
