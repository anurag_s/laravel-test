{!! Form::submit('Save', ['class' => 'btn btn-save']) !!}
<a href="{{ route('contacts.index') }}" class="btn btn-warning btn-cancel">Cancel</a>
<div class="form">

    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
        {!! Form::label('first_name', 'First Name') !!}
        {!! Form::text('first_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('first_name') }}</small>
    </div>

   <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
   {!! Form::label('last_name', 'Last Name') !!} <br>
   {!! Form::text('last_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
   <small class="text-danger">{{ $errors->first('last_name') }}</small>
   </div>

   <div class="form-group{{ $errors->has('account_name') ? ' has-error' : '' }}">
    {!! Form::label('account_name', 'Account Name') !!} <br>
    {!! Form::text('account_name', null, ['class' => 'form-control', 'id' => 'account_search']) !!}
    <small class="text-danger">{{ $errors->first('Account_name') }}</small>

    {!! Form::hidden('account_id', 'value', ['id' => 'account_id']) !!}
    </div>

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        {!! Form::label('email', 'Email') !!}
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('email') }}</small>
    </div>

    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
        {!! Form::label('phone', 'Phone') !!}
        {!! Form::text('phone', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('phone') }}</small>
    </div>

    <div class="form-group{{ $errors->has('wa_verification') ? ' has-error' : '' }}">
        {!! Form::label('wa_verification', 'WAVerification') !!} <br>
        {!! Form::select('wa_verification', ['' => '', 'Valid' => 'Valid', 'Invalid' => 'Invalid', 'NotVerified' => 'NotVerified'], ['required' => 'required', 'multiple'], ['class' => 'form-control select']) !!}
        <small class="text-danger">{{ $errors->first('industry') }}</small>
    </div>

    <div class="form-group{{ $errors->has('customer_type') ? ' has-error' : '' }}">
        {!! Form::label('customer_type', 'Customer Type') !!} <br>
        {!! Form::select('customer_type', ['' => '', 'Customer' => 'Customer', 'Aggregate Partner' => 'Aggregate Partner', 'Competitor' => 'Competitor', 'Courier Company' => 'Courier Company', 'End User' => 'End User', 'Evolve Partner' => 'Evolve Partner', 'Partner' => 'Partner', 'Supplier' => 'Supplier'], ['id' => 'account_name', 'required' => 'required', 'multiple'], ['class' => 'form-control select']) !!}
        <small class="text-danger">{{ $errors->first('customer_type') }}</small>
    </div>

    <div class="form-group{{ $errors->has('lead_source') ? ' has-error' : '' }}">
        {!! Form::label('lead_source', 'Lead Source') !!} <br>
        {!! Form::select('lead_source', ['' => '', 'Advertisement' => 'Advertisement', 'Campaign' => 'Campaign', 'Cross Selling' => 'Cross Selling', 'Courier Company' => 'Courier Company', 'Email_Campaign' => 'Email Campaign', 'Event' => 'Event', 'Existing Customer' => 'Existing Customer', 'Facebook' => 'Facebook', 'Google Adwards' => 'Google Adwards', 'Justdial' => 'Justdial', 'LinkedIn' => 'LinkedIn', 'Longitude_App' => 'Longitude App', 'Other' => 'Other', 'Partner' => 'Partner', 'Reference' => 'Reference', 'Sampark_Setu' => 'Sampark Setu', 'SoftwareSuggest' => 'Software Suggest', 'Tally_Partner' => 'Tally Partner', 'Webinar' => 'Webinar', 'WhatsAPP' => 'WhatsAPP' , 'Customer Reference' => 'Customer Reference'], ['id' => 'account_name', 'required' => 'required', 'multiple'], ['class' => 'form-control select']) !!}
        <small class="text-danger">{{ $errors->first('customer_type') }}</small>
    </div>

    <div class="form-group{{ $errors->has('designation') ? ' has-error' : '' }}">
        {!! Form::label('designation', 'Designation') !!}
        {!! Form::text('designation', null, ['class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('designation') }}</small>
    </div>

    <div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">
        {!! Form::label('department', 'Department') !!}
        {!! Form::text('department', null, ['class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('department') }}</small>
    </div>

    <div class="form-group{{ $errors->has('hierarchy') ? ' has-error' : '' }}">
        {!! Form::label('hierarchy', 'Hierarchy') !!} <br>
        {!! Form::select('hierarchy', ['' => '', '00' => '00 - Boss, Proprietor, MD, CEO, Director, VP, (Anyone who is in top management)',
                                                 '01' => '01 - VP Sales Manager', 
                                                 '02' => '02 - Sales Manager / Team Leader', 
                                                 '03' => '03 - Sales Executive', 
                                                 '11' => '11 - VP Technical, CTO, IT Head, IT manager etc.', 
                                                 '12' => '12 - Jr IT Manager, or Technical Team Leader',
                                                 '13' => '13 - Technical Executive, Support Engineer, Support Executive', 
                                                 '21' => '21 - CFO', 
                                                 '22' => '22 - Sr. Accountant', 
                                                 '23' => '23 - Account Executive', 
                                                 '99' => '99 - Other'], ['id' => 'account_name', 'required' => 'required', 'multiple'], ['class' => 'form-control select']) !!}
        <small class="text-danger">{{ $errors->first('hierarchy') }}</small>
    </div>

    <div class="form-group{{ $errors->has('legend_code') ? ' has-error' : '' }}">
        {!! Form::label('legend_code', 'Legend Code') !!} <br>
        {!! Form::select('legend_code', ['' => '', 'Yellow' => 'Yellow', 'Red' => 'Red', 'Green' => 'Green'], ['id' => 'account_name', 'required' => 'required', 'multiple'], ['class' => 'form-control select']) !!}
        <small class="text-danger">{{ $errors->first('legend_code') }}</small>
    </div>

    <div class="form-group{{ $errors->has('address_verified') ? ' has-error' : '' }}">
        {!! Form::label('address_verified', 'Address Verified') !!} <br>
        {!! Form::checkbox('address_verified', 'yes', 'false', ['id' => 'address_verified', 'multiple'], ['class' => 'form-check check-address-verified']) !!}
        <small class="text-danger">{{ $errors->first('address_verified') }}</small>
    </div>

    <div class="form-group{{ $errors->has('followup_date') ? ' has-error' : '' }}">
        {!! Form::label('followup_date', 'Followup Date') !!}
        {!! Form::text('followup_date', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('followup_date') }}</small>
    </div>

    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        {!! Form::label('description', 'Description') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control description', 'rows' => '3']) !!}
        <small class="text-danger">{{ $errors->first('description') }}</small>
    </div>


    <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
        {!! Form::label('comment', 'Comment') !!}
        {!! Form::textarea('comment', null, ['class' => 'form-control comments', 'rows' => '3']) !!}
        <small class="text-danger">{{ $errors->first('comment') }}</small>
    </div>

    <div class="form-group{{ $errors->has('assigned_to') ? ' has-error' : '' }}">
    {!! Form::label('assigned_to', 'Assigned To') !!}
    {!! Form::text('assigned_to', null, ['class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('assigned_to') }}</small>
    </div>
    
</div>
