@extends('layouts.app')

@section('links')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
@endsection

@section('content')
{!! Form::open(['route' => 'contacts.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
@csrf
@include('contacts.form')
{!! Form::close() !!}
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $('input[name="followup_date"]').daterangepicker({
            drops: 'up',
            timePicker: true,
            singleDatePicker: true,
            locale: {
                format: 'DD/MM/YYYY hh:mm A'
            },
    });
    
$("#account_search").autocomplete({
    source: function(request, response) {
        $.ajax({
            url:"{{route('accounts.getAccounts')}}",
            type: 'post',
            dataType: "json",
            data: {
               _token: CSRF_TOKEN,
               search: request.term
            },
            success: function(data) {
               response(data);
            }
          });
    },
    select: function(event, ui) {
        $('#account_search').val(ui.item.label);
        $('#account_id').val(ui.item.value); 
        return false;
    },
    focus: function(event, ui){
        $('#account_search').val(ui.item.label);
        $('#account_id').val(ui.item.value); 
        return false;
    },
    change: function(event, ui) {
        if(ui.item == null) {
            event.currentTarget.value = ''; 
            event.currentTarget.focus();
        }
    }
});
</script>

@endsection
