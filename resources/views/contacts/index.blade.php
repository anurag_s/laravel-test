@extends('layouts.app')

@section('links')
 <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css"> 
@endsection
@section('content')
<div class="container">
    <table class="table table-bordered data-table" width="100%">
        <thead>
            <tr>
                <th>Contact Name</th>
                <th>Account Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Designation</th>
                <th>Department</th>
                <th>Hierarchy</th>
                <th>Customer Type</th>
                <th>Assigned To</th>
                <th>Created At</th>
                <th>Updated At</th>
            </tr>
        </thead>
        {{-- @foreach ($contacts as $contact)
            @foreach ($contact->accounts as $account)
                @dump($account->account_name)
            @endforeach
        @endforeach --}}
        {{-- <thead class="columnSearch">
            <tr>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Relationship</th>
                <th>Industry</th>
                <th>Website</th>
                <th>GSTIN</th>
                <th>Assigned To</th>
                <th>LinkedIn</th>
                <th>Account Status</th>
                <th>Created At</th>
                <th>Updated At</th>
            </tr>
        </thead> --}}
        <tbody>
            
        </tbody>
    </table>
</div>
@endsection
@section('scripts')
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js" ></script>
<script> 
    //  $('.data-table .columnSearch th').each(function () {
    //     var title = $(this).text();
    //     $(this).html('<input type="text" placeholder="Search ' + title + '" />');
    // });
     var table = $('.data-table').DataTable({
            processing: true,
            serverSide: false,
            // ajax: {
            //     url: "{{ route('accounts.index') }}",
            //     //dataSrc: 'data'
            // },
            ajax:  "{{ route('contacts.index') }}",
            columns: [
                {data: 'name', name: 'name'},
                {data: 'account_name', name: 'account_name'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'designation', name: 'designation'},
                {data: 'department', name: 'department'},
                {data: 'hierarchy', name: 'hierarchy'},
                {data: 'customer_type', name: 'customer_type'},
                {data: 'assigned_to', name: 'assigned_to'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'},
            ],
        scrollX: true,
        });
</script>

@endsection