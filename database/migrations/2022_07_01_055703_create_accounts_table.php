<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('account_name')->nullable();
            $table->string('account_relationship')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('industry')->nullable();
            $table->string('website')->nullable();
            $table->string('linkedln_url')->nullable();
            $table->string('gstin')->nullable();
            $table->string('pan')->nullable();
            $table->string('account_status')->nullable();
            $table->text('description')->nullable();  
            $table->text('comments')->nullable();
            $table->text('cosummary')->nullable();
            $table->string('assigned_to')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
