<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('wa_verification')->nullable();
            $table->string('customer_type')->nullable();
            $table->string('lead_source')->nullable();
            $table->string('designation')->nullable();
            $table->string('department')->nullable();
            $table->string('hierarchy')->nullable();
            $table->string('legend_code')->nullable();
            $table->string('address_verified')->nullable();
            $table->dateTime('followup_date')->nullable();
            $table->text('description')->nullable();
            $table->string('comment')->nullable();
            $table->string('assigned_to')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
